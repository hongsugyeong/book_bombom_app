import 'package:book_bombom_app/model/book_item.dart';
import 'package:flutter/material.dart';

class ComponentBookItem extends StatelessWidget {
  const ComponentBookItem({
    super.key,
    required this.bookItem,
    required this.callback
  });

  final BookItem bookItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Image.asset(
            bookItem.imgUrl,
            width: 160,
            height: 160,
          ),
          Column(
            children: [
              Text(
                bookItem.bookTitle,
                style: TextStyle(
                fontSize: 11,
                  fontWeight: FontWeight.bold
              ),
              textAlign: TextAlign.center,
              ),
              Text('${bookItem.bookPrice}원',
              style: TextStyle(
                fontSize: 10
              ),
              textAlign: TextAlign.center,
              ),
            ],
          )
        ],
      ),
    );
  }
}
