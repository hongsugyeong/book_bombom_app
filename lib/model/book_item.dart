class BookItem {
  num id;
  String imgUrl;
  String bookTitle;
  num bookPrice;

  BookItem(this.id, this.imgUrl, this.bookTitle, this.bookPrice);
}