import 'package:flutter/material.dart';
import 'package:book_bombom_app/model/book_item.dart';

class PageBookDetail extends StatefulWidget {
  const PageBookDetail({
    super.key,
    required this.bookItem
  });

  final BookItem bookItem;
  
  @override
  State<PageBookDetail> createState() => _PageBookDetailState();
}

class _PageBookDetailState extends State<PageBookDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text('${widget.bookItem.id}번째 책'),
            Image.asset(widget.bookItem.imgUrl),
            Text(
              widget.bookItem.bookTitle,
              style: TextStyle(
                fontWeight: FontWeight.bold
              ),
            ),
            Text(
              '${widget.bookItem.bookPrice}원',
              style: TextStyle(
                fontSize: 10
              ),
            )
          ],
        ),
      ),
    );
  }
}

