import 'package:book_bombom_app/components/component_book_item.dart';
import 'package:book_bombom_app/model/book_item.dart';
import 'package:book_bombom_app/pages/page_book_detail.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class PageIndexI extends StatefulWidget {
  const PageIndexI({super.key});

  @override
  State<PageIndexI> createState() => _PageIndexIState();
}

class _PageIndexIState extends State<PageIndexI> {
  final List<String> _list = ['assets/M1.jpeg', 'assets/M2.jpeg'];

  List<BookItem> _item = [
    BookItem(1, 'assets/XL1.jpeg', '지긋지긋한 사람을 없애지 않고 죽이는 법', 100),
    BookItem(2, 'assets/XL2.jpeg', '인생의 태도', 100),
    BookItem(3, 'assets/XL3.jpeg', '사라진 것들', 100),
    BookItem(4, 'assets/XL4.jpeg', '서른의 불만 마흔의 불안', 100),
    BookItem(5, 'assets/XL5.jpeg', '귀신들의 땅', 100),
    BookItem(6, 'assets/XL6.jpeg', '투자의 정석', 100),
    BookItem(7, 'assets/XL7.jpeg', '서른에 읽는 아들러', 100),
    BookItem(8, 'assets/XL8.jpeg', '언제나 네 곁에 있을게', 100),
  ];

  final CarouselController carouselController = CarouselController();
  int currentIndex = 0;

  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:
            IconButton(onPressed: () {}, icon: Icon(Icons.menu)), // 왼쪽 메뉴버튼
        title: Text('BOMBOM'), // 타이틀
        centerTitle: true, // 타이틀 텍스트를 가운데로 정렬 시킴
        actions: [
          // 우측의 액션 버튼들
          IconButton(onPressed: () {}, icon: Icon(Icons.search))
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        onTap: (value) {
          setState(() {
            _index = value;
            print(_index);
          });
        },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.recommend), label: 'Recommend'),
          // BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Popularity'),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_rounded), label: 'mypage'),
        ],
      ),
      body: _build(context),
    );
  }

  Widget _build(BuildContext context) {
    return Container(
      child: Container(
        child: Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                  height: 180.0,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 60),
                  onPageChanged: ((index, reason) {
                    setState(() {
                      currentIndex = index;
                    });
                  })),
              items: _list.map((String item) {
                return Image.asset(item, fit: BoxFit.contain);
              }).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _list.asMap().entries.map((entry) {
                return Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () {
                      carouselController.animateToPage(entry.key);
                    },
                    child: Container(
                      width: 8.0,
                      height: 8.0,
                      margin: const EdgeInsets.symmetric(
                          vertical: 45.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: const Color.fromARGB(255, 133, 133, 133)
                              .withOpacity(
                                  currentIndex == entry.key ? 0.9 : 0.4)),
                    ),
                  ),
                );
              }).toList(),
            ),
            Container(
              height: 90,
              child: Column(
                children: [
                  Text(
                    '닿을 수 있는 세상',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    '닿지 못 하는 세상에 대해서',
                    style: TextStyle(
                      fontSize: 10,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
                child: Expanded(
                    child: GridView.builder(
                      itemCount: _item.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1 / 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return ComponentBookItem(
                            bookItem: _item[index],
                            callback: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      PageBookDetail(bookItem: _item[index])));
                            });
                      },
                    ))
            )

          ],
        ),
      ),
    );
  }
}
